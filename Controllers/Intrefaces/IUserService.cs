namespace FirstApp.Controllers
{
    public interface IUserService {
         User getUser(string username);
         User createUser(string username,string password);
         string loginUser(string username,string password);


    }
}