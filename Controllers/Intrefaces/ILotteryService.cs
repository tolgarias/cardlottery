using System;
using System.Collections.Generic;

namespace FirstApp.Controllers
{
    public interface ILotteryService {
        DrawResult play(List<int> cards, long bet);

        long getNextDrawTime();

    }
}