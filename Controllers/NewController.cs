using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using MongoDB.Bson;
namespace FirstApp.Controllers
{
    public class NewController : Controller
    {

        private async Task<User> findUser(string name){
            await Task.Delay(10);
            var user = findUserFromDB(name);
            if(user == null){
                await findUser(name);
            }
            return user;
        }
        private User findUserFromDB(string name){
            var client = new MongoClient();
            var mongoDB = client.GetDatabase("game");
            var tmpCollection = mongoDB.GetCollection<User>("users");
            var filter = Builders<User>.Filter.Eq("Username",name);
            var user = tmpCollection.Find(filter).ToList();
            if(user.Count>0){
                return user[0];
            }
            
            return null;
        }

        [HttpGet("User/add")]
        public User addUSer(){
            var client = new MongoClient();
            var mongoDB = client.GetDatabase("game");
            var tmpCollection = mongoDB.GetCollection<User>("users");
            User user = new User(){
                Username = "tolgarias",
                Password = "121231312",
                Chips = 190000
            };
            tmpCollection.InsertOne(user);
        
            return user;
        }

        [HttpGet("User/get/{name}")]
        public async Task<IActionResult> getUser(string name){
            int timeout = 5000;
            var task = findUser(name);
            if(await Task.WhenAny(task,Task.Delay(timeout)) == task){
                return new ObjectResult(task.Result);
            }
            return NotFound();
        }
    }
    
}
