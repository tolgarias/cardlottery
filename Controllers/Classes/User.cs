using MongoDB.Bson;

namespace FirstApp.Controllers
{
    public class User  {
        public ObjectId id {get;set;}
        public string Username {get;set;}
        public string  Password {get;set;}
        public long Chips {get;set;}
    }
}