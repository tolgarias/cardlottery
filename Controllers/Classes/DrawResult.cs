namespace FirstApp.Controllers
{
    public class DrawResult {
        public int Card {get;set;}
        public int WinnedChip {get;set;}
    }
}